% --- ENSSAT_OFDM.m 
% --- 
% Description 
%	  Script for OFDM signal generation and decoding 
%	  User parameters are defined at the beginning of the script 
%	  Some parts have to be completed. See lab subject
% --- 
% Syntax 
%     ENSSAT_OFDM
% ---
% Variables 
%
% --- 
% v 1.0 - Robin Gerzaguet.


% % --- Clear all stuff 
% clearvars; 
% close all;
% clc; 

addpath('./path');
addpath('./path/e310_tools');


% ---------------------------------------------------- 
%% --- User parameters  
% ---------------------------------------------------- 
% --- Transmission parameters
modulation			= 'OFDM';				% --- Waveform ['OFDM']
sizeConstellation	= 4;					% --- Constellation size [ 2 : BSPK, 4 : QPSK, 16 : 16-QAM]

% --- Param??tres de l'OFDM
sizeFFT				= 1024;					% --- Number of OFDM subcarriers  (i.e FFT size)
typePrefixe			= 'CP';					% --- Type of Cyclic prefix used 
													% --- 'none'	: No prefix used 
													% --- 'guard'	: Guard intervall
													% --- 'cp'		: Cyclic prefix 
													% --- 'cs'		: Cyclic suffix
sizePrefix			= 72;					% --- Size of prefix (for none, it is automatically overwritten to 0)
% --- Channel parameters
freq                = 15.36e6;              % --- LTE 10MHz sampling frequency
channelTap			= [1];                  % --- Channel tap coefficients
snrChannel			= 90;					% --- Signal to Noise ratio (dB)
% --- Additionnal Parameters
nbOFDMSymbols		= 14*10;				% --- Number of OFDM symbols
overSamp			= 1; 					% --- Oversampling factor for RF stage (1 for OFDM)
subcarriers			= allocateLTE(sizeFFT);                  % --- Allocated subcarriers
													%	1:sizeFFT	: All subcarriers allocated
													%	[3]			: Only subcarrier 3 is enable 
													% allocateLTE(sizeFFT): returns the default Broadband LTE allocation

% ---------------------------------------------------- 
%% --- Tx Stage
% ----------------------------------------------------                                                
tx_stage;


% ---------------------------------------------------- 
%% --- Receiver
% ----------------------------------------------------                                                



                                                   




