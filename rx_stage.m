% --- rx_stage.m 
% --- 
% Description 
%	  Script for OFDM  decoding 
% --- 
% Syntax 
%     rx_stage
%
% --- 
% v 1.0 - Robin Gerzaguet.




% ---------------------------------------------------- 
%% --- OFDM decoding  
% ---------------------------------------------------- 
% --- Prepare signal 
% In the end, we shall have a matrix of decoded QAM symbols 
qamRx	  = zeros(nbOFDMSymbols,nbSubcarriers);			% --- Before equalisation 
qamRxEq	  = zeros(nbOFDMSymbols,nbSubcarriers);			% --- After equalisation
% ---------------------------------------------------- 
% ---------------------------------------------------- 
% !!!!!!!  TODO !!!!!!!!!!!!!
% Create the OFDM demodulator
% ----------------------------------------------------


% ---------------------------------------------------- 
%% --- Binary decoding  
% ---------------------------------------------------- 
% --- From Matrix constellation, deduce bit sequence 
bitRx	  = bitUnMapping(qamRx(:),sizeConstellation);
% --- Calculate performance indicators 
ber		  = sum(xor(bitRx,bitSeq(1:length(bitRx)))) / length(bitRx);
fprintf('OFDM simulation \n BER = %f \n',ber);
if snrChannel > 80 && ber ~=0
    fprintf('OFDM demodulator not functionnal\n');
end
viewConstellation(qamRx);
