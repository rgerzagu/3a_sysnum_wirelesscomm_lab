% --- channel.m 
% --- 
% Description 
%	  Multipath channel model 
% --- 
% Syntax 
%     channel
%
% --- 
% v 1.0 - Robin Gerzaguet.


% ---------------------------------------------------- 
%% --- Channel model
% ---------------------------------------------------- 
% --- Channel is constant, let's do convolution 
sigChan		  = conv(sigPrefix,channelTap);
% --- Calculate average power 
puissIn		  = 1/length(sigChan)*sum(abs(sigChan).^2);


% ---------------------------------------------------- 
%% --- Prepare additive white gaussian noise 
% ----------------------------------------------------
% --- We have a SNR, not Eb/No 
% In non full alloc, that's a problem. Let's evaluate spectral efficiency and do 
% some shenaniggans to have something related to what bits see in terms of information theory
%FIXME Not debugged channel power: maybe adding  "/ sqrt(sum(abs(channelTap).^2)) " ?
ebNo =10*log10( 10.^(snrChannel/10) * (sizeFFT + sizePrefix) / (log2(sizeConstellation) * nbSubcarriers)); 
% --- Generates a white additive circular gaussian noise 
whiteNoise=sqrt(puissIn)/sqrt(2)*10^(-ebNo/20)*(randn(1,length(sigChan))+1i*randn(1,length(sigChan)));	
% --- Adding AWGN 
sigRx	  = sigChan + whiteNoise;
