% --- tx_stage.m 
% --- 
% Description 
%	  Script for OFDM signal generation
% --- 
% Syntax 
%     tx_stage
% ---
%
% --- 
% v 1.0 - Robin Gerzaguet.




% ---------------------------------------------------- 
%% --- Binary Sequence Generation
% ---------------------------------------------------- 
% --- Finding how many subcarriers we have 
nbSubcarriers	  = length(subcarriers);
% --- Calculating number of transmitted bits (to map T/F grid)
% Numbit should be equal to (nbSubcarriers * nbSymb * bitPerSymbol)
numBits			  = log2(sizeConstellation) * nbSubcarriers * nbOFDMSymbols;
% --- Binary sequence generation 
bitSeq			  = floor(2*rand(1,numBits));
% ---------------------------------------------------- 
% --- Binary to complex constellation 
% ---------------------------------------------------- 
% --- Binary Sequence mapping to constellation 
% This stage transforms binary sequence into gray coded constellation 
qamSeq			  = bitMapping(bitSeq,sizeConstellation);
% --- Serial to parallel transformation 
% OFDM is a T/F waveform: transform dataflow into (nbOFDMSymbols x nbSubcarriers) matrix 
qamMat			  = reshape(qamSeq,nbOFDMSymbols,nbSubcarriers);


% ---------------------------------------------------- 
%% --- OFDM waveform generation stage 
% ----------------------------------------------------
% --- We need to map qamMat on the complete frequency mapping 
qamAll			= zeros(nbOFDMSymbols,sizeFFT);
qamAll(:,subcarriers) = qamMat;
% --- OFDM symbols size 
% Depending on prefix, calculate size 
switch typePrefixe
	case 'none'
		ofdmSymbolSize	= sizeFFT;
	otherwise 
		ofdmSymbolSize	= sizeFFT + sizePrefix;
end
% --- Init vectors 
sigPrefix		= zeros(1, nbOFDMSymbols * ofdmSymbolSize);	% --- Vector with prefix 
sigNoPrefix		= zeros(1, nbOFDMSymbols * sizeFFT);	% --- Vector with prefix 
% --- Frequency mapping 
for iN = 1 : 1 : nbOFDMSymbols
    % --- Serial to parallel and frequency mapping
    currSymb = ifftshift(ifft(qamAll(iN,:)));
    % --- Parallel to serial
    sigNoPrefix( (iN-1)*sizeFFT + (1:sizeFFT)) = currSymb;
end
% --- Adding prefix 
switch lower(typePrefixe)
	case 'none'
		% --- No Prefix used 
		sigPrefix	  = sigNoPrefix ;	
	case 'guard'
		% --- Inserting Zeros
		for n = 1 : 1 : nbOFDMSymbols
			sigPrefix(1+ (n-1)*(sizeFFT+sizePrefix):n*(sizeFFT+sizePrefix)) = [zeros(1,sizePrefix) sigNoPrefix(1+ (n-1)*sizeFFT:n*sizeFFT)];
		end
	case 'cp'
        % --- Inserting CP (append end to beginning)
		for n = 1 : 1 : nbOFDMSymbols
			cpCurr	  = sigNoPrefix(1+ (n-1)*sizeFFT  + (sizeFFT-sizePrefix):n*sizeFFT);
			sigPrefix(1+ (n-1)*(sizeFFT+sizePrefix):n*(sizeFFT+sizePrefix)) = [cpCurr sigNoPrefix(1+ (n-1)*sizeFFT:n*sizeFFT)];
		end	
	case 'cs'
        % --- Inserting CS (append beginning to end)
		for n = 1 : 1 : nbOFDMSymbols
			csCurr	  = sigNoPrefix(1+ (n-1)*sizeFFT  + (1:sizePrefix));
			sigPrefix(1+ (n-1)*(sizeFFT+sizePrefix):n*(sizeFFT+sizePrefix)) = [sigNoPrefix(1+ (n-1)*sizeFFT:n*sizeFFT) csCurr];
		end	
end

% ---------------------------------------------------- 
%% --- Signal at the output of the transmitter  
% ---------------------------------------------------- 
sigTx	= sigPrefix;
