% --- hardConstellation.m 
% --- 
% Description 
%    Returns a constellation with same size as the input with raw decision based on minimuml euclidean distance. Can be used as a raw EVM measure if the noise level is sufficiently low (i.e no errors)
% --- 
% Syntax 
%		constHard	= hardConstellation(noiseConstellation,bPs);
%					  % --- Input parameters 
%							  noiseConstellation  : Constellation with noise 
%							  bPs				  : Bit per symbol 
%					  % --- Output parameters 
%							  constHard			  : Raw constellation
% --- 
% v 1.0 - Robin Gerzaguet.

function constHard	= hardConstellation(noiseConstellation,bPS)
	scaleF	= sqrt(2/3*(2^bPS-1));
	% --- Switch on map size 
	switch bPS
		case 1
			% --- BPSK sign decision 
			constHard = sign(noiseConstellation) / scaleF;
		case 2
			% --- QPSK sign decision 
			constHard = (sign(real(noiseConstellation)) + 1i * sign(imag(noiseConstellation))) / scaleF;
		case 4 
			% --- 16-QAM            			
			bounds = [-sqrt(2)/2 0 sqrt(2)/2];
			% --- Calibration of output constellation after decision
			floorConstI = (1 + double(real(noiseConstellation)>bounds(1)) + double(real(noiseConstellation)>bounds(2)) + double(real(noiseConstellation)>bounds(3)));

			floorConstQ = (1+ double(imag(noiseConstellation)>bounds(1)) + double(imag(noiseConstellation)>bounds(2)) + double(imag(noiseConstellation)>bounds(3)));
			alphabet	= [-3 -1 1 3]/scaleF;
			constHard	= alphabet(floorConstI) + 1i*alphabet(floorConstQ);
		case 6 
			bounds = [-6:2:6]/(sqrt(2/3*63));
			% --- Calibration of output constellation after decision
			floorConstI = zeros(length(noiseConstellation),1);
			floorConstQ = zeros(length(noiseConstellation),1);
			for i=1:1:length(bounds)
				floorConstI = floorConstI + double(real(noiseConstellation(:))>bounds(i));
				floorConstQ  = floorConstQ + double(imag(noiseConstellation(:))>bounds(i));
			end
			alphabet = [-7 -5 -3 -1 1 3 5 7]/scaleF;
			constHard	= alphabet(1+floorConstI) + 1i*alphabet(1+floorConstQ);
	end
end
