% --- displayOFDMBase.m 
% --- 
% Description 
%    Display exponential base for OFDM modulation with numPorteuses carrier 
% --- 
% Syntax 
%    displayOFDMBase(numPorteuses)
% --- 
% v 1.0 - Robin Gerzaguet 

function displayOFDMBase(numPorteuses)
	% --- Prepare figure 
	figure('name','Database of exponential');
	abscis	= linspace(0,1,400);
	abscis2	= linspace(0,1,numPorteuses);
	dF		= 1;
	%	for i = 1 : 1 : numPorteuses
	%		complexBase	  = exp( 2*1i*pi* i*dF/numPorteuses * abscis);
	%		subplot(numPorteuses,2,(i-1)*2+1)
	%		plot(abscis,real(complexBase),'lineWidth',2,'LineStyle','-','color','blue');
	%		hold on
	%		grid minor;
	%		xlabel('Temps [s]');
	%		title(['Base pour la porteuse : ' num2str(i)]);
	%		subplot(numPorteuses,2,(i-1)*2+2)
	%		plot(abscis,imag(complexBase),'lineWidth',2,'LineStyle','-','color','red');
	%		hold on
	%		grid minor;
	%		xlabel('Temps [s]');
	%		title(['Base pour la porteuse : ' num2str(i)]);
	%end

	%	nF	  = 1;
	%	nrows = 4;
	%	ncols = 4;
	%	for ii = 1 : 1 : numPorteuses
	%		complexBase	  = exp( 2*1i*pi* (ii-1)*dF * abscis);
	%		if nrows*ncols-mod(-ii, nrows*ncols) == 1
	%			figure('name',['Carrier Constellation : Figure ' num2str(nF)]);
	%			nF	  = nF +1;
	%		end
	%		h(ii) = subplot(nrows, ncols, nrows*ncols-mod(-ii, nrows*ncols));
	%		plot(abscis,real(complexBase),'lineWidth',2,'LineStyle','-','color','blue');
	%		hold on
	%		plot(abscis,imag(complexBase),'lineWidth',2,'LineStyle','-','color','red');
	%%		legend('Reel','imag');
	%		hold on
	%		grid minor;
	%		xlabel('Temps (fraction du temps utile Tu)');
	%		title(['Base pour la porteuse : ' num2str(ii)]);
	%		ylim([-1 1]);
	%	end

	for ii = 1 : 1 : numPorteuses
		complexBase	  = (1+1i)*(ii) + 1/3* exp( 2*1i*pi* (ii-1)*dF * abscis);
		complexBase2	  = (1+1i)*(ii) + 1/3* exp( 2*1i*pi* (ii-1)*dF * abscis2);

		a1=subplot(1,2,1);
		plot(abscis,real(complexBase),'lineWidth',2,'LineStyle','-','color','blue');
		hold on
		plot(abscis2,real(complexBase2),'lineWidth',2,'LineStyle','none','marker','*','markersize',6,'color','blue');
		a2=subplot(1,2,2);
		plot(abscis,imag(complexBase),'lineWidth',2,'LineStyle','-','color','red');
		hold on
		plot(abscis2,imag(complexBase2),'lineWidth',2,'LineStyle','none','marker','*','markersize',6,'color','red');
	end
	subplot(1,2,1)
	xlabel('Temps (fraction du temps utile Tu)');
	ylabel('Numero de la porteuse');
	title('Partie reelle');
	legend('Analogique',['Numerique pour ' num2str(numPorteuses) ' porteuses ']);
	ylim([0 numPorteuses+2]);
	subplot(1,2,2)
	xlabel('Temps (fraction du temps utile Tu)');
	ylabel('Numero de la porteuse');
	legend('Analogique',['Numerique pour ' num2str(numPorteuses) ' porteuses ']);
	ylim([0 numPorteuses+2]);
	title('Partie imaginaire');
	set(a1,'YGrid','on');
	set(a2,'YGrid','on');
end


