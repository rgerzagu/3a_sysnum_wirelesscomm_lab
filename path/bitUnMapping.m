% --- bitUnMapping.m
% ---
% Description
%		Recover bit Sequence from Mapped symbols
% ---
% Syntax
% 		[bitSeq] = bitUnMapping(mappedSymbol,modForm);
% 		seqMapped	: Mapped Constellation
% 		bitSequence	: Sequence of bits to be mapped
% 		modForm		: Size of constellation 2^(bit per symbol)
% ---
% v 1.0 - Jan. 2013 - Robin Gerzaguet.

function bitSeq = bitUnMapping(mappedSymbol,modForm)
    switch modForm
		case 2
			bitSeq=(mappedSymbol > 0).';
        case 4
            % --- QPSK UnMapping
            bitSeq=zeros(1,2*length(mappedSymbol));
            bitSeq(1:2:end)= real(mappedSymbol)>0;
            bitSeq(2:2:end)= imag(mappedSymbol)>0;
        case 16
            % --- 16 QAM UnMapping
            % --- Bounds
            bounds = [-sqrt(2)/2 0 sqrt(2)/2];
            % --- Calibration of output constellation after decision
            floorConstI = (1 + double(real(mappedSymbol)>bounds(1)) + double(real(mappedSymbol)>bounds(2)) + double(real(mappedSymbol)>bounds(3)));
            floorConstQ = (1+ double(imag(mappedSymbol)>bounds(1)) + double(imag(mappedSymbol)>bounds(2)) + double(imag(mappedSymbol)>bounds(3)));
            % --- Gray Code
%              grayencod = bitxor(0:4-1, floor((0:4-1)/2));
			grayencod = [0 1 3 2];
            % --- Sequence of bits
            bitI=dec2bin(grayencod(floorConstI'),2);
            bitQ=dec2bin(grayencod(floorConstQ'),2);
            % --- Conversion to array
            bitSeq1=zeros(1,2*length(bitQ));
            bitSeq2=zeros(1,2*length(bitQ));
            % the provide vectors are char, and they have to be converted
            % to double. 48 is the 0 value in ascii mode so '0'- 48=0 and
            % '1' - 48 =1 .
            % --- I Path
            tmpI=bitI - 48;
            bitSeq1(1:2:end)=tmpI(:,1);
            bitSeq1(2:2:end)=tmpI(:,2);
            % --- Q Path
            tmpQ=bitQ -48;
            bitSeq2(1:2:end)=tmpQ(:,1);
            bitSeq2(2:2:end)=tmpQ(:,2);           
            % --- Recreation of sequence
            bitSeq=zeros(1,2*length(bitSeq1));
            bitSeq(1:2:end)=bitSeq1;
            bitSeq(2:2:end)=bitSeq2;
        case 64
            % --- 64 QAM Unmapping
            % --- Bounds
            bounds = [-6:2:6]/(sqrt(2/3*63));
            % --- Calibration of output constellation after decision
            floorConstI = zeros(length(mappedSymbol),1);
            floorConstQ = zeros(length(mappedSymbol),1);
            for i=1:1:length(bounds)
                floorConstI = floorConstI + double(real(mappedSymbol(:))>bounds(i));
                floorConstQ = floorConstQ + double(imag(mappedSymbol(:))>bounds(i));
            end
            % --- Gray Code
            grayencod=[0 1 3 2 7 6 4 5];
            % --- Sequence of bits
            bitI=dec2bin(grayencod(floorConstI+1),3);
            bitQ=dec2bin(grayencod(floorConstQ+1),3); 
            % the provide vectors are char, and they have to be converted
            % to double. 48 is the 0 value in ascii mode so '0'- 48=0 and
            % '1' - 48 =1 .
            bitSeq1=zeros(1,3*length(bitQ));
            bitSeq2=zeros(1,3*length(bitQ));
            % --- I Path
            tmpI=bitI - 48;
            bitSeq1(1:3:end)=tmpI(:,1);
            bitSeq1(2:3:end)=tmpI(:,2);
            bitSeq1(3:3:end)=tmpI(:,3);
            % --- Q Path
            tmpQ=bitQ -48;
            bitSeq2(1:3:end)=tmpQ(:,1);
            bitSeq2(2:3:end)=tmpQ(:,2);
            bitSeq2(3:3:end)=tmpQ(:,3);  
            % --- Recreation of sequence
            bitSeq=zeros(1,2*length(bitSeq1));
            bitSeq(1:2:end)=bitSeq1;
            bitSeq(2:2:end)=bitSeq2;            
    end
end
