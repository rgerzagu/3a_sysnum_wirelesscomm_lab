% --- allocateLTE.m 
% --- 
% Description 
% Returns the default broadband allocation for a LTE-10MHz system 
% This allocation corresponds to the allocation for 3GPP-LTE Release 14.
% Input parameter is the targeted LTE size 
% Output parameter is a vector of size nAlloc (where nAlloc is the number of allocated subcarriers) with index of enabled subcarriers
% --- 
% Syntax 
%    allocatedSubcarriers	  = allocateLTE(sizeFFT,pilots)
%						% --- Input parameters 
%							  sizeFFT	: LTE FFT size (release 14) {128,256,521,1024,1536,2048}
%                             pilots    : Index of subcarrier reserved for
%                             pilot
%						% --- Output parameters 
%							  allocatedSubcarriers : Vector of allocated subcarriers 
% --- 
% v 1.0 - Robin Gerzaguet.

function allocatedSubcarriers	  = allocateLTE(sizeFFT,pilots)
	% --- Getting RB allocation 
	% Use simRF system for ressource allocation based on RB alloc and subcarrier shift 
	[blockShift,vectRB]		=	getLTEAlloc(sizeFFT);
	% --- Convert this into one vector of enable subcarriers
	% Release 14 -> RB size is 12 subcarriers
	allocatedSubcarriers    = allocateRB(vectRB,blockShift,12,sizeFFT);
    % --- From this removing the allocated subcarriers with pilots
    if nargin == 1
        % --- No second argument, no pilot inserted
        pilots = [];
    end
    allocatedSubcarriers    = setdiff(allocatedSubcarriers,pilots);
end
