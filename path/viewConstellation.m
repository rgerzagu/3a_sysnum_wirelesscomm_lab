function ah=viewConstellation(varargin)
% viewConstellation2(varargin). Display constellation of variables :
% viewConstellation2(var1,var2,...,varN)

if nargin==0
    error('simRF:argChek','function must have at least 1 argument');
end


col=[1 0 0;0 0 1];
if nargin>2
    col=hsv(nargin);
end
hsv(nargin);
ah=figure('name','Constellation');
hold on;
grid on;
str=cell(1,nargin);
for i=1:1:nargin
    in=varargin{i};
    plot(in(:),'color',col(i,:),'MarkerSize',15,'LineStyle','none','Marker','.');
    title('constellation');
    str{i}=inputname(i);
end
legend(str);
end