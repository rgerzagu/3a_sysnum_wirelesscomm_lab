% --- displayChannel.m 
% --- 
% Description 
%    Display frequency and phase response of a Multi-tap channel H
% --- 
% Syntax 
%    displayChannel(H)
% --- 
% v 1.0 - Robin Gerzaguet


function displayChannel(H,overSamp,numPorteuses)
	% --- Getting overSampling if not in parameters
	if nargin == 1
		overSamp = 1;
	end
	% --- Prepare figure
	figure('Name','Channel response');
	% --- For Magnitude
	subplot(2,1,1)
	grid minor;
	hold on 
	% ---------------------------------------------------- 
	% --- Display Frequency 
	% ---------------------------------------------------- 	
	sFFT  = 512 * overSamp;
	absci	= overSamp*(-0.5+(0:sFFT-1)/sFFT);
	plot(absci,abs(fftshift(fft(H.',sFFT))),'lineWidth',3,'LineStyle','-','color','blue');
	xlabel('Normalized frequency');
	ylabel(' | H |');
	title('Frequency Response of Channel');
	zz	= axis;
	zz	= [-0.5 0.5 0 zz(4)];
	axis(zz);
	plot([-0.5 0.5],[0 0],'lineWidth',2,'LineStyle','-','color','black');
	% --- Adding Carrier frequencies 
	if nargin == 3
		vectC	= -0.5 : 1 / numPorteuses : 0.5 - 1/numPorteuses;
		for i = 1 : 1 : length(vectC)
			plot([vectC(i) vectC(i)],[zz(3) zz(4)],'lineWidth',1,'LineStyle','--','color','black'); 
			text(vectC(i),zz(4),['P : ' num2str(1+mod(i-1 - numPorteuses/2,numPorteuses))]);
		end
	end

	% ---------------------------------------------------- 
	% --- Display Phases 
	% ---------------------------------------------------- 
	subplot(2,1,2);
	grid minor;
	hold on 
	% --- Frequency response
	plot(absci,angle(fftshift(fft(H.',sFFT))),'lineWidth',3,'LineStyle','-','color','red');
	xlabel('Normalized frequency');
	ylabel(' Phase ');
	title('Phase  Response of Channel');
	% --- Adding Carrier frequencies 
	if nargin == 3
		vectC	= -0.5 : 1 / numPorteuses : 0.5 - 1/numPorteuses;
		zz		= axis;
		for i = 1 : 1 : length(vectC)
			plot([vectC(i) vectC(i)],[zz(3) zz(4)],'lineWidth',1,'LineStyle','--','color','black'); 
			text(vectC(i),zz(4),['P : ' num2str(1+mod(i-1 - numPorteuses/2,numPorteuses))]);
		end
	end
	zz  = axis;
	zz	= [-0.5 0.5 zz(3) zz(4)];
	axis(zz);
	plot([-0.5 0.5],[0 0],'lineWidth',2,'LineStyle','-','color','black');
end
