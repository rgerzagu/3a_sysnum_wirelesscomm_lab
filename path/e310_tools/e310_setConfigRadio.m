% --- e310_setConfigRadio.m 
% --- 
% Description 
%    Set e310 Radio configuration with Matlab/Octave. The bandwidth, carrier frequency, Rx gain can be modified on the fly with a XMLPRC server (combined with external system python call)
% --- 
% Syntax 
%		status = e310_setConfigRadio(USRP_Address,type,'parameter',argument, ...)freq,samp_rate,rx_gain,bufferSize);
%						% --- Input parameters 
%							  USRP_Address	: USRP IP address 
%							  type			: 'Tx' or 'Rx' link adaptation
%							  parameter		: string of parameter to update 
%										  freq			: Carrier frequency 
%										  samp_rate		: bandwidth 
%										  gain			: Tx/Rx gain 
%							  value			: Value of parameter 
% --- 
% v 1.0 - Robin Gerzaguet.


function status = e310_setConfigRadio(varargin)
	% --- Extract parameters 
	USRP_Address  = varargin{1};
	typeCo		  = varargin{2};
	% --- Getting dir 
	cD	 = pwd; 
	% --- Getting dir of this function assuming python is in the same path 
	spE		= mfilename();
	spD		= mfilename('fullpath');
	ind		= strfind(spD,spE);
	spF		= spD(1:ind-1);
	cd(spF);
	% --- create system string 
	switch lower(typeCo)
		case 'tx'
			% --- Number of parameters to pass 
			nbArg	= floor(nargin - 2)/2; 
			strAll	=  ['python e310_updateMetaDataTx.py --USRP_Address=' USRP_Address];
			for iA	= 1 : 1 : nbArg
				strg  = varargin{2+(iA-1)*2+1};		  % --- Parameter 
				strV  = varargin{2+(iA-1)*2 +2};	  % --- Its value 
				switch strg 
					% --- From argument convert into python option 
					case 'freq' 
						aS	= '-f ';
					case 'samp_rate'
						aS	= '-s ';
					case 'gain'
						aS	= '-r ';
					otherwise 
						error('simRF:e310_setConfigRadio','Unknown parameter to pass to e310');
				end
				strAll = [strAll ' ' aS num2str(strV)];
			end
			callS	= strAll;
		case 'rx'
			% --- Number of parameters to pass 
			nbArg	= floor(nargin - 2)/2; 
			strAll	=  ['python e310_updateMetaDataRx.py --USRP_Address=' USRP_Address];
			for iA	= 1 : 1 : nbArg
				strg  = varargin{2+(iA-1)*2+1};		  % --- Parameter 
				strV  = varargin{2+(iA-1)*2 +2};	  % --- Its value 
				switch strg 
					% --- From argument convert into python option 
					case 'freq' 
						aS	= '-f ';
					case 'samp_rate'
						aS	= '-s ';
					case 'gain'
						aS	= '-r ';
					otherwise 
						error('simRF:e310_setConfigRadio','Unknown parameter to pass to e310');
				end
				strAll = [strAll ' ' aS num2str(strV)];
			end
			callS	= strAll;
		otherwise 
			error('simRF:e310_setConfigRadio','typeCo must be Tx or Rx');
	end
	octave	 = isOctave();
	if octave 
		% --- Octave external call 
		status	= system(callS,0);
	else 
		% --- Matlab extrernal call 
		status	= system(callS);
	end
	% --- Go back to initial dir 
	cd(cD);
end
