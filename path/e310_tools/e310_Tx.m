% --- e310_Tx.m 
% --- 
% Description 
%    Script to transmit OFDM signal with e310 device
% --- 
% Syntax 
%    e310_Tx
% --- 
% v 1.0 - Robin Gerzaguet.


% ---------------------------------------------------- 
%% --- OFDM configuration  
% ---------------------------------------------------- 
ENSSAT_OFDM 

% ---------------------------------------------------- 
%% --- Init radio  
% ---------------------------------------------------- 
% --- AIR config 
USRP_Address  = '192.168.10.11';
% --- USRP parameters 
carrierFreq    = 868e6;
freq           = 1.94e6;
gainTx         = 70;
% --- Sending configuration
e310_setConfigRadio(USRP_Address,'Tx','freq',carrierFreq);		  % --- Update carrier frequency 
e310_setConfigRadio(USRP_Address,'Tx','samp_rate',freq);	  % --- Update bandwidth 
e310_setConfigRadio(USRP_Address,'Tx','gain',gainTx);     % --- Update Tx analog gain

% ---------------------------------------------------- 
%% --- Creating signal  
% ---------------------------------------------------- 
tx_stage;	  % --- Creating sigTx

% --- Emulate mulitpath channel 
% It's fun
firTap  = [1 0 0 0.1 0 0 0 0.05];
sigTx   = filter(firTap,1,sigTx);


% --- Full scale 
scalingFactor   = 1 / max(abs(sigTx));
sigId           = sigTx * scalingFactor;



% ---------------------------------------------------- 
%% --- Send data to USRP  
% ---------------------------------------------------- 
e310_sendData(USRP_Address,sigId)

