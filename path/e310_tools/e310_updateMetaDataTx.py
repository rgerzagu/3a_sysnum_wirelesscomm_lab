#
############
# e310_updateMetaDataTx.py 
############
# Robin Gerzaguet.

import xmlrpclib 
from optparse import OptionParser


def updateConfig(samp_rate,USRP_Address,tx_gain,freq,bufferSize):
    # Create XMLRPC system 
    xmlAddress  = 'http://'+USRP_Address+':30000';
    # Sampling rate update 
    if samp_rate != (-1):
        client_samp_rate     = xmlrpclib.Server(xmlAddress);
        client_samp_rate.set_samp_rate(samp_rate)
    # Carrier frequency update 
    if freq != (-1):
        client_freq     = xmlrpclib.Server(xmlAddress);
        client_freq.set_freq(freq)
    # Analog Tx gain update 
    if tx_gain != (-1):
        client_rx_gain     = xmlrpclib.Server(xmlAddress);
        client_rx_gain.set_tx_gain(tx_gain)
    


def main(options=None):
    # Parameter update
    parser = OptionParser( usage="%prog: [options]")
    parser.add_option("-u", "--USRP_Address", dest="USRP_Address", type="string", default='192.168.10.11',
            help="Set USRP IP address [default=%default]")
    parser.add_option("-s", "--samp_rate", dest="samp_rate", type="float", default=-1,
            help="Set sampling rate [default=%default]")
    parser.add_option("-f", "--freq", dest="freq", type="float", default=-1,
            help="Set carrier frequency [default=%default]")
    parser.add_option("-r", "--tx_gain", dest="tx_gain", type="float", default=-1,
            help="Set Tx gain [default=%default]")  
    parser.add_option("-b", "--bufferSize", dest="bufferSize", type="int", default=-1,
            help="Set Buffer size [default=%default]")  
    (options, args) = parser.parse_args()
    # Launching method for config update 
    updateConfig(samp_rate=options.samp_rate,USRP_Address=options.USRP_Address,tx_gain=options.tx_gain,freq=options.freq,bufferSize=options.bufferSize)



if __name__ == '__main__':
    main()


