% --- e310_sendData .m 
% --- 
% Description 
%    Send samples to e310 device trough ZMQ socket 
% --- 
% Syntax 
%    [sigRx,nbS]  = e310_sendData(socket,data);
%				  % --- Input parameters 
%						USRP_Address	: IP address of USRP
%						data	: Complex vector with data to transmit
%				  % --- Output parameters 
%						flag	: Flag for socket communication
%								  : -1	: fail to send data 
% --- 
% v 1.0 - Robin Gerzaguet.


function [flag]  = e310_sendData(USRP_Address,data)
% --- Matlab or Octave 
octave	= isOctave();
if octave 
	% --- Create file to transmit 
	writeComplexBinary(data,'hostSSH.dat');
	% --- Send file with SSH link 
	callS	= ['scp hostSSH.dat root@' USRP_Address ':/home/root/Transmitter/'];
	status	= system(callS,0);
else 
	% --- Create file to transmit 
	writeComplexBinary(data,'hostSSH.dat');
	% --- Send file with SSH link 
	callS	= ['scp hostSSH.dat root@' USRP_Address ':/home/root/Transmitter/'];
	status	= system(callS);
end
end
