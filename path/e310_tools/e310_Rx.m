% --- e310_Rx.m 
% --- 
% Description 
%    Script to perfrom HIL simuation with Matlab 
% --- 
% Syntax 
%    e310_Rx
% --- 
% v 1.0 - Robin Gerzaguet.


% ---------------------------------------------------- 
%% --- OFDM configuration 
% ---------------------------------------------------- 
ENSSAT_OFDM;

% ---------------------------------------------------- 
%% --- Radio parameters  
% ---------------------------------------------------- 
% --- Init connection  
carrierFreq         = 868e6;
freq                = 1.92e6;
gainRx              = 16;
% --- Update e310 configuration 
USRP_Address        = '192.168.10.13';					          % --- Setting IP address 
e310_setConfigRadio(USRP_Address,'Rx','freq',carrierFreq);		  % --- Update carrier frequency 
e310_setConfigRadio(USRP_Address,'Rx','samp_rate',freq);	      % --- Update bandwidth 
e310_setConfigRadio(USRP_Address,'Rx','gain',gainRx);             % --- Update Rx analog gain


% ---------------------------------------------------- 
%% --- Iterative decoding  
% ---------------------------------------------------- 
nbAcqui	  = 25;
doPlot	  = 1;
maxBufferSize = 64000;
targetLenght = nbOFDMSymbols * 2;
for iac = 1 : 1 : nbAcqui
	%% ---------------------------------------------------- 
	%%% --- Samples acquisition 
	% ---------------------------------------------------- 
	% --- Getting samples 
	clear socket;
	socket	    = e310_initConnection(USRP_Address);
	[sigRxInit]	= e310_getData(socket,maxBufferSize);
	e310_closeConnection(socket);
	if length(sigRxInit) < targetLenght + (sizeFFT+sizePrefix)*(nbOFDMSymbols-1) 
		% --- Signal is too narrow, try to relaunch 
		pause(0.2);
	else 
		fprintf('Acquisition -- %d \t',iac);
		% --- Number of symbol to decode 
		nbOFDMSymbols	= floor(length(sigRxInit) / (sizeFFT + sizePrefix)); 
		% --- Preparing Rx stage 
        sigTx = sigRxInit;
		rx_stage; 
        constHard	= hardConstellation(qamRxEq(:),log2(sizeConstellation));
        evm			= 10*log10( mean( abs(qamRxEq(:)-constHard(:)).^2));
        fprintf("with EVM= %2.2f dB \n",evm);
		% --- Plotting 
		if doPlot
			% --- Plotting PSD 
            figure(1);
			subplot(3,2,[1 2]);
			sFFT  = 2048;
			fPSD  = (abs(fftshift(fft(sigRxInit,sFFT))));
			xAx	  = freq*[0:sFFT-1]/sFFT - freq/2;
			plot(xAx,20*log10(fPSD/max(fPSD)));
			xlabel('Frequency (Hz)');
			ylabel('Magnitude');
			title('Spectrum of received signal');
			ylim([-60 10]);
			grid;
            % --- Channel 
			subplot(3,2,[3 4])
            sFFT  = length(subcarriers);
            xAx	  = freq*[0:sFFT-1]/sFFT - freq/2;
            plot(xAx,10*log10(abs(fftshift(allChan))/ max(abs(allChan))));
            grid;
            ylim([-5 3]);
            xlabel('Frequency');
            ylabel('Channel magnitude [dB]');
			% --- Plotting constellations
			subplot(3,2,5)
			plot(qamRx(:),'o');
			title('Before equalization');
			grid;
			subplot(3,2,6);
			plot(qamRxEq(:),'o');
			title('After equalisation');
			axis([-2 2 -2 2]);
			xlabel('Real part');
			ylabel('Imag part');
			grid;
            pause(0.1)
		end
	end
end
