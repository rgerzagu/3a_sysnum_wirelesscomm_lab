#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# GNU Radio Python Flow Graph
# Title: Host_xmlprc_server
# Author: R. Gerzaguet
# Generated: Tue Jul 17 11:04:29 2018
# GNU Radio version: 3.7.12.0
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import xmlrpclib


class Host_xmlprc_server(gr.top_block):

    def __init__(self, samp_rate=0, USRP_Address='192.168.10.11', rx_gain=0, freq=0,bufferSize=1):
        gr.top_block.__init__(self, "Host_xmlprc_server")
        ##################################################
        # Parameters
        ##################################################
        self.samp_rate = samp_rate
        self.USRP_Address = USRP_Address
        self.rx_gain = rx_gain
        self.freq = freq
        self.bufferSize = bufferSize
        ##################################################
        # Blocks
        ##################################################
        xmlAddress = 'http://'+USRP_Address+':30000';
        self.xmlrpc_client0_0 = xmlrpclib.Server(xmlAddress)
        self.xmlrpc_client0 = xmlrpclib.Server(xmlAddress);
        self.xmlrpc_client = xmlrpclib.Server(xmlAddress);
        ##################################################
        # Manual update: Update USRP config
        ##################################################
        self.set_USRP_Address(USRP_Address)
        self.set_samp_rate(samp_rate)
        self.set_freq(freq)
        self.set_rx_gain(rx_gain)
        self.set_bufferSize(bufferSize)

    def get_bufferSize(self):
        return self.bufferSize

    def set_bufferSize(self, bufferSize):
        self.bufferSize =bufferSize 
        #self.xmlrpc_client0_0.set_bufferSize(self.bufferSize)


    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        print self.samp_rate

    def get_USRP_Address(self):
        return self.USRP_Address

    def set_USRP_Address(self, USRP_Address):
        self.USRP_Address = USRP_Address

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain
        self.xmlrpc_client0.set_rx_gain(self.rx_gain)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.xmlrpc_client.set_freq(self.freq)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.xmlrpc_client0_0.set_samp_rate(self.samp_rate)


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    return parser


def main(top_block_cls=Host_xmlprc_server, options=None):
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    parser.add_option("-u", "--USRP_Address", dest="USRP_Address", type="string", default='192.168.10.11',
            help="Set USRP IP address [default=%default]")
    parser.add_option("-s", "--samp_rate", dest="samp_rate", type="float", default=2e6,
            help="Set sampling rate [default=%default]")
    parser.add_option("-f", "--freq", dest="freq", type="float", default=900e6,
            help="Set carrier frequency [default=%default]")
    parser.add_option("-r", "--rx_gain", dest="rx_gain", type="float", default=0,
            help="Set Rx gain [default=%default]")  
    parser.add_option("-b", "--bufferSize", dest="bufferSize", type="int", default=1,
            help="Set Buffer size [default=%default]")  
    (options, args) = parser.parse_args()
    tb = top_block_cls(samp_rate=options.samp_rate,USRP_Address=options.USRP_Address,rx_gain=options.rx_gain,freq=options.freq,bufferSize=options.bufferSize)
    tb.start()
    tb.stop()
    #try:
        #raw_input('Press Enter to quit: ')
    #except EOFError:
        #pass
    #tb.stop()
    #tb.wait()


if __name__ == '__main__':
    main()
