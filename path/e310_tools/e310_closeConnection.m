% --- e310_closeConnection.m 
% --- 
% Description 
%    Close ZMQ sokcet with e310 
% --- 
% Syntax 
%    status = e310_closeConnection(socket)
%				% --- Input parameters 
%					  socket : ZMQ socket to close 
%				% --- Output parameters 
%					  status   : status of closed socket
% --- 
% v 1.0 - Robin Gerzaguet.

function e310_closeConnection(socket) 
	% --- Matlab or Octave 
	octave = isOctave();
	if octave 
		zmq_close(socket);
	else 
		zmq.core.close(socket.socket);
        zmq.core.ctx_term(socket.context);
%         zmq.core.ctx_shutdown(socket.context);
	end
end
