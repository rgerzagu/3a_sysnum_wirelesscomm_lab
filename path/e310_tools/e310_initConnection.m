% --- e310_initConnection.m 
% --- 
% Description 
%    Create ZMQ socket for e310 communication with Matlab/Octave
% --- 
% Syntax 
%		socket	= e310_initConnection(e310_address,type);
%					% --- Input parameters 
%							  e310_address	: Adress of e310 peripheral ('192.168.10.x')
%							  type			: 'Rx' or 'Tx' (default 'Rx');
%					% --- Output parameters 
%							  socket		: structure of ZMQ socket 
% --- 
% v 1.0 - Robin Gerzaguet.


function socket = e310_initConnection(varargin)
% --- Check parameters
if nargin == 1
    typeCo = 'Rx';
else
    typeCo = varargin{2};
end
% --- Octave or Matlab mode
octave = isOctave();
if octave
    pkg load instrument-control
    pkg load zeromq
    pkg load signal
    switch lower(typeCo)
        case 'rx'
            e310_address  = varargin{1};
            tcpSys	= ['tcp://' e310_address ':9999'];
            socket	= zmq_socket('ZMQ_SUB');
            zmq_connect(socket,tcpSys);
            zmq_setsockopt(socket,ZMQ_SUBSCRIBE,'');
        case 'tx'
            e310_address  = varargin{1};
            % --- Create socket
            tcpSys	= ['tcp://192.168.10.10'  ':9999'];
            socket	= zmq_socket('ZMQ_PUSH');
            status = zmq_connect(socket,tcpSys);
            socket.USRP_Address = e310_address;
    end
else
    switch lower(typeCo)
        case 'rx'
            e310_address  = varargin{1};
            tcpSys	      = ['tcp://' e310_address ':9999'];
            context       = zmq.core.ctx_new();
            socket        = zmq.core.socket(context, 'ZMQ_SUB');
            zmq.core.connect(socket, tcpSys);
            zmq.core.setsockopt(socket,'ZMQ_SUBSCRIBE','');
            struct.socket = socket;
            struct.context = context;
            socket = struct;
        case 'tx'
            
    end
end
end
