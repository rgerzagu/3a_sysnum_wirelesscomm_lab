% --- e310_getData.m 
% --- 
% Description 
%    Gets samples from e310 device trough ZMQ socket 
% --- 
% Syntax 
%    [sigRx,nbS]  = e310_getData(socket,nbS);
%				  % --- Input parameters 
%						socket	: ZMQ e310 socket 
%						nbS		: Targeted number of symbols 
%						mode	: Connection mode 
%								  - single : get one socket (possibility not full) 
%				  % --- Output parameters 
%						sigRx	: complex vector of acquired signal 
%						nbS		: Length of acquired signal 
% --- 
% v 1.0 - Robin Gerzaguet.


function [sigRx,nbS]  = e310_getData(socket,nbS)
	% --- Matlab or octave ? 
	octave = isOctave();
	if octave
		% --- Getting buffer size 
		buffSize	= nbS * 8 * 1;
		% Buffer size is desired sequence x 8 (uInt -> double) x 2 (I/Q)
		%	fprintf(' --- Prepare to acquire data ..');
		% --- Create ZMQ socket 
		sigRxUint= zmq_recv(socket,buffSize,0);
		% --- Data is uint -> convert to float 
		dataRx = (typecast (uint8 (sigRxUint), 'single'));
		% --- Separate I and Q streams 
		sigRx = dataRx(1:2:end) + 1i*dataRx(2:2:end);
		% --- Getting length 
		nbS	  = length(sigRx);
	else 
		buffSize	= nbS * 8 * 1;
		% Buffer size is desired sequence x 8 (uInt -> double) x 2 (I/Q)
		%	fprintf(' --- Prepare to acquire data ..');
		% --- Create ZMQ socket
        warning off 
		sigRxUint= zmq.core.recv(socket.socket,buffSize);
        warning on
		% --- Data is uint -> convert to float 
		dataRx = (typecast (uint8 (sigRxUint), 'single'));
		% --- Separate I and Q streams 
		sigRx = double(dataRx(1:2:end)) + 1i*double(dataRx(2:2:end));
		% --- Getting length 
		nbS	  = length(sigRx);
	end
end
