% --- writeFloatBinary.m
% --- 
% Description
%	  Convert a MATLAB files to a binary file for implementing in a GNU-Radio companion chain	
% ---
% Syntax
%		  [stream] = writeFloatBinary(data,fileName)
%			  % ---inputs :
%				data	: data to be modified (can be a file .mat)
%				fileName: Output binary name of file
% ---
% v 2.0 - Oct. 2013 - Robin Gerzaguet. 


function [stream] = writeFloatBinary(data,fileName)
	% --- Check if data is a mat files or a vector
	if isa(data,'char');
		% --- Check that file is a mat file
		if isempty(data,'.mat')
			error('simRF:writeFloatBinary','If Data is a mat files, it must a .mat file');
		else
			% --- OK, try to load file
			try
				data =load(data);
			catch
				% --- Raise exception
				error('simRF:writeFloatBinary','Data files to be imported not found');
			end
		end
	end
	% --- Write data in binary mode
	fid = fopen(fileName,'w');
	if fid <0
		% --- Unable to open file
		warning('simRF:writeFloatBinary','Unable to open the file');
	else
		% --- Write stream
		stream = fwrite(fid,data,'float');
		fclose(fid);
%	end
end


